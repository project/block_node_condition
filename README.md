INTRODUCTION
------------

This module enables site builders to control block visibility with nodes
(selecting on which nodes block shoud be displayed).

INSTALLATION
------------

 - Install the module as you would normally install a contributed Drupal
   module. Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

- Enable block_node_condition module.
- Go to /admin/structure/block and select the block you wish to configure.
- Under "Visibility" you should see new tab "Nodes".
- Configure the "Display block only on selected nodes" by selecting existing
nodes.


MAINTAINERS
-----------

  - Borut Piletic (borutpiletic) - https://www.drupal.org/u/borutpiletic
